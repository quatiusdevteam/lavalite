<?php

namespace Litepie\User\Traits\Permissions;

/**
 * Trait InteractsWithPermissions.
 */
trait InteractsWithPermissions
{
	
    protected static function bootModelTrait()
    {
        // there should be a 'retrieving' model event
        static::saving(function ($model) {
            $model->permissions = is_array($model->permission)?json_encode($model->permissions):$model->permissions;
        });
        // to support eager loading before fetching 
        // from the database, to save a second call like this:

        // static::retrieved(function ($model) {
        //     $model->permissions = $model->permissions?json_decode($model->permissions, true):[];
        // });
    }

    // public function getPermissionsAttribute(){
    //     return isset($this->getAttributes()['permissions'])?json_decode($this->getAttributes()['permissions'], true):[];
    // }
	
    /**
     * Attach the given permission.
     *
     * @param array|\Litepie\User\Permission $permission
     * @param array                          $options
     */
    public function attachPermission($permission, array $options = [])
    {
        if (!is_array($permission)) {
            if ($this->existPermission($permission->name)) {
                return;
            }
        }

        $this->permissions()->attach($permission, [
            'value'   => array_get($options, 'value', true),
            'expires' => array_get($options, 'expires', null),
        ]);
    }

    /**
     * Get the a permission using the permission name.
     *
     * @param string $permissionName
     *
     * @return bool
     */
    public function existPermission($permissionName)
    {
        $permission = $this->permissions->first(function ($key, $value) use ($permissionName) {
            return $value->name == $permissionName;
        });

        if (!empty($permission)) {
            $active = (is_null($permission->pivot->expires) or $permission->pivot->expires->isFuture());

            if ($active) {
                return (bool) $permission->pivot->value;
            }
        }

        return false;
    }

    /**
     * Alias to the detachPermission method.
     *
     * @param \Litepie\User\Permission $permission
     *
     * @return int
     */
    public function revokePermission($permission)
    {
        return $this->detachPermission($permission);
    }

    /**
     * Detach the given permission from the model.
     *
     * @param \Litepie\User\Permission $permission
     *
     * @return int
     */
    public function detachPermission($permission)
    {
        return $this->permissions()->detach($permission);
    }

    /**
     * Sync the given permissions.
     *
     * @param array $permissions
     *
     * @return array
     */
    public function syncPermissions(array $permissions)
    {
        return $this->permissions()->sync($permissions);
    }

    /**
     * Revoke all user permissions.
     *
     * @return int
     */
    public function revokePermissions()
    {
        return $this->permissions()->detach();
    }

    /**
     * Revoke expired user permissions.
     *
     * @return int|null
     */
    public function revokeExpiredPermissions()
    {
        $expiredPermissions = $this->permissions()->wherePivot('expires', '<', Carbon::now())->get();

        if ($expiredPermissions->count() > 0) {
            return $this->permissions()->detach($expiredPermissions->modelKeys());
        }
    }

    /**
     * Extend an existing temporary permission.
     *
     * @param string $permission
     * @param array  $options
     *
     * @return bool|null
     */
    public function extendPermission($permission, array $options)
    {
        foreach ($this->permissions as $_permission) {
            if ($_permission->name === $permission) {
                return $this->permissions()->updateExistingPivot(
                    $_permission->id,
                    array_only($options, ['value', 'expires'])
                );
            }
        }
    }
}
