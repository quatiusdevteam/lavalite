<?php

namespace Litepie\User\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Route;
use Request;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot()
    {

        if (Request::is('*admin/user/user*')) {
            Route::bind('user', function ($id) {
                $user = $this->app->make('Litepie\Contracts\User\UserRepository');
                return $user->findOrNew($id);
            });
        }

        if (Request::is('*admin/user/role*')) {
            Route::bind('role', function ($id) {
                $role = $this->app->make('Litepie\Contracts\User\RoleRepository');

                return $role->findOrNew($id);
            });
        }

        if (Request::is('*admin/user/permission*')) {
            Route::bind('permission', function ($id) {
                $permission = $this->app->make('Litepie\Contracts\User\PermissionRepository');

                return $permission->findOrNew($id);
            });
        }

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map()
    {
        Route::group(['namespace' => $this->namespace], function ($router) {
            require __DIR__ . '/../Http/routes.php';
        });
    }

}
