<?php

namespace Litepie\Menu\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Node\Traits\SimpleNode;
use Litepie\Trans\Traits\Trans;

class Menu extends Model
{
    use Hashids, Slugger, Trans, SoftDeletes, SimpleNode;

     /**
      * Configuartion for the model.
      *
      * @var array
      */
     protected $config = 'menu.menu';
     
     function update(array $attributes = [], array $options = [])
     {
     	if (isset($attributes['roles']))
     	{
     		$attributes['role'] = json_encode($attributes['roles']);
     		unset($attributes['roles']);
     	}
     	
     	if (isset($attributes['permissions']))
     	{
     		$attributes['permission'] = json_encode($attributes['permissions']);
     		unset($attributes['permissions']);
     	}
     	
     	return parent::update($attributes,$options);
     }
     
     function getRolesAttribute($value)
     {
     	if ($this->role == "")
     		return [];
     	
     	return json_decode($this->role,true)?:[$this->role];
     }
     
     function setRolesAttribute($list)
     {
     	$this->role = json_encode($list);
     	$this->_roles = $list;
     }
     
     function getPermissionsAttribute($value)
     {
     	if ($this->permission== "")
     		return [];
     		
     	return json_decode($this->permission,true)?:[$this->permission];
     }
     
     function setPermissionsAttribute($list)
     {
     	$this->permission= json_encode($list);
     }
     
     public function checkAccess($user)
     {
     	if ($this->status == 0)
     		return false;
     		
     	$roles = $this->roles;
     	if ($roles && !$user->hasRoles($roles))
     		return false;
     	
     	$permissions = $this->permissions;
     	
     	if ($permissions)
     	{
	     	foreach ($permissions as $permission)
	     	{
	     		if ($user->hasPermission($permission))
	     			return true;
	     	}
	     	
	     	return false;
     	}
     	
     	return true;
	 }
}
