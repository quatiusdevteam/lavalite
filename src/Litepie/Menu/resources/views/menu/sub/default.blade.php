<ul class="dropdown-menu {{ $menus->active or '' }} animated fadeIn" role="menu">

    @foreach ($menus as $menu)
		<?php if(!$menu->checkAccess(user())) continue;?>
		
	 	@if ($menu->description)
	 		{!! Theme::blader($menu->description, ['menu' => $menu]) !!}
	 	@else
	 		@if ($menu->hasChildren())
			    <li class="dropdown {{ $menu->active or '' }}">
			        <a href="{{$menu->url=='#'? '#': trans_url($menu->url)}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			            <i class="{{{ $menu->icon or '' }}}"></i> <span>{{$menu->name}}</span>
			             <span class="caret"></span>
			        </a>
			        @include('public::menu.sub.main', array('menus' => $menu->getChildren()))
			    </li>
			    @else
			    <li class="{{ $menu->active or '' }}">
			        <a href="{{$menu->url=='#'? '#': trans_url($menu->url)}}" target="{{$menu->target}}">
			            <i class="{{{ $menu->icon or '' }}}"></i>
			            <span>{{$menu->name}}</span>
			        </a>
			    </li>
	   		@endif
	    @endif
    @endforeach
</ul>